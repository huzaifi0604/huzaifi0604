### Hi there 👋
My name is Muhammad Huzaifa and

* 🔭 I’m currently enrolled in bachelors program for Cyber Security & Digital Forensics.
* 👨‍⚖️ Cyber Defense and Network Security Intern at Thincscorp 
* 🧑‍💻 Trainer at Cyber Space Legion at FAST-NUCES
* 👯 I’m looking to collaborate on
     - Security Related Projects
     - Software/ Mobile App Development
     - Programming in C++, C, Java, x86 Assembly
* 💬 Ask me about Anything
* 📫 How to reach me
                    huzzaifaasim@gmail.com
* I would like to connect with you on
  - Linkedin : https://www.linkedin.com/in/muhammad-huzaifa-707b43226
* 😄 Pronouns: He
<img src="https://github.com/SABERGLOW/SABERGLOW/blob/master/Misc/image%20backups/homeycombs/C.svg"/>
